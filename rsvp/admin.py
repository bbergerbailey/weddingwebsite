from django.contrib import admin
from rsvp.models import Guest, Event, Food_preference
# Register your models here.
@admin.register(Guest)
class GuestAdmin(admin.ModelAdmin):
    list_display=[
        "id",
        "first_name",
        "last_name",
        "email",
        "plus_one",
        "event"
    ]

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display=[
        "id",
        "name",
        "dateTime",
        "location",
    ]

@admin.register(Food_preference)
class Food_preferenceAdmin(admin.ModelAdmin):
    list_display=[
        "is_vegan",
        "is_vegetarian",
        "is_gluten_free",
        "will_drink",
        "other",
        "guest",
    ]
