from django.db import models

# Create your models here.

class Event(models.Model):
    name = models.CharField(max_length=30)
    dateTime = models.DateTimeField(auto_now_add=False, null=True)
    location = models.CharField(max_length=50)

class Guest(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    plus_one = models.BooleanField(default=False)
    event = models.ForeignKey(Event,
                              related_name="guests",
                              on_delete = models.CASCADE)

class Food_preference(models.Model):
    is_vegan = models.BooleanField(default=False)
    is_vegetarian = models.BooleanField(default=False)
    is_gluten_free = models.BooleanField(default=False)
    will_drink= models.BooleanField(default=True)
    other = models.TextField()
    guest = models.ForeignKey(Guest,
                              related_name="food_preferences",
                              on_delete = models.CASCADE)
